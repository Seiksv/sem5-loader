//
//  StringConstants.swift
//  
//
//  Created by Seiks on 6/3/22.
//

import Foundation

class StringConstants {
    static let jsonDataURL = "https://raw.githubusercontent.com/phunware/dev-interview-homework/master/feed.json"
    static let conectionSuccess = "Conectado"
    static let conectionFailure = "Sin conexion"
    static let contentLoadByCache = "Contenido cargado desde cache"
    static let contentLoadFailureByCache = "No hay data desde cache"
    static let contentLoadByInternet = "Contenido cargado desde internet"
    static let helveticaFont = "HelveticaNeue-Bold"
    static let hiraginoFont = "Hiragino Sans"
    static let title = "Titulo: "
    static let noImage = "No image on this episode"
}
