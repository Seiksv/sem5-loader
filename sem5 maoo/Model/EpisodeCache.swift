//
//  EpisodeCache.swift
//  sem5 maoo
//
//  Created by Seiks on 6/3/22.
//

import Foundation
import Reachability
import CodableCache

final class EpisodeCache {
    
    let cache: CodableCache<[Episodes.episode]>
    
    init(cacheKey: AnyHashable) {
        cache = CodableCache<[Episodes.episode]>(key: cacheKey)
    }
    
    func getEpisodes() -> [Episodes.episode]? {
        return cache.get()
    }
    
    func set(episode: [Episodes.episode]) throws {
        try cache.set(value: episode)
    }
}
