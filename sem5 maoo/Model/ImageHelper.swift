    //
    //  ImageHelper.swift
    //  sem5 maoo
    //
    //  Created by Seiks on 6/3/22.
    //

import Foundation
import PromiseKit

class ImageHelper {
    
    let cache = URLCache.shared

    func getImage(imageURL: URL) -> Promise<UIImage> {
        
        let request = URLRequest(url: imageURL)
        
        //Si se tiene cache entonces vamos a buscar la cache y la mostramos, sino se descarga y se guarda en cache
        if (cache.cachedResponse(for: request) != nil) {
            return showFromCache(imageURL: imageURL)
        } else {
            return showFromInternet(imageURL: imageURL)
        }
    }

    func showFromInternet(imageURL: URL) -> Promise<UIImage> {
        
        return Promise { seal in
            //La request sera la llave con la cual se guardara la imagen en cache
            let request = URLRequest(url: imageURL)
            
            //Se hace la peticion asincronica para obtener la imagen desde la url
            DispatchQueue.global(qos: .background).async {
                //Se crea la URLSessionDataTask de la peticion para extraer la imagen
                let dataTask = URLSession.shared.dataTask(with: imageURL) { data, response, _ in
                    //Valido el tipo de respuesta
                    if let httpResponse = response as? HTTPURLResponse {
                        print("Status code \(httpResponse.statusCode)")
                        //Si el status es 200 se guarda la cache, sino muestro un placeholder
                        if httpResponse.statusCode == 200 {
                        //Si viene la informacion deseada se procede a guardad en la cache
                            if let data = data {
                                //'cacheData' es la informacion que se desea guardar en la cache
                                let cachedData = CachedURLResponse(response: response!, data: data)
                                //se almacena en la cache la informacion de 'cacheData' junto con la peticion que sera la llave
                                self.cache.storeCachedResponse(cachedData, for: request)
                                
                                //Se indica que la promesa se cumplio, entonces indicamos el valor de la imagen para ser mostrada en la vista
                                seal.fulfill(UIImage(data: data)!)
                                print("Saved in Cache \(imageURL)")
                            }
                        } else {
                            self.cache.removeCachedResponse(for: request)
                            seal.fulfill(UIImage(named: "noImage")!)
                        }
                    }
                }
                //Se inicializa la URLSessionDataTask con esta llamada
                dataTask.resume()
            }
        }
    }

    func showFromCache(imageURL: URL) -> Promise<UIImage> {
        return Promise { seal in
            
            let request = URLRequest(url: imageURL)
            
            //Se hace la peticion asincronica para obtener la imagen desde la cache
            DispatchQueue.global(qos: .background).async {
                //Se valida primero si existe la cache con la llave que es "request" y si es una imagen valida de UIImage
                if let data = self.cache.cachedResponse(for: request)?.data, let image = UIImage(data: data) {
                    
                    //En un hilo diferente, se devuelve o despacha la peticion que esta en cola
                    DispatchQueue.main.async {
                    //Se indica que la promesa se cumplio, entonces indicamos el valor de la imagen para ser mostrada en la vista
                        seal.fulfill(image)
                        print("Reading by Cache \(imageURL)")
                    }
                }
            }
        }
    }
}
