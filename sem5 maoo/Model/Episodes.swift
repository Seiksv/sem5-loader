//
//  dataHelper.swift
//  sem5 maoo
//
//  Created by Seiks on 3/3/22.
//

import Foundation
import PromiseKit
import CodableCache


class Episodes {
    struct episode: Codable {
        let id: Int
        let description: String
        let title: String
        let timestamp: String
        let image: String?
        let date: String
        let locationline1: String
        let locationline2: String
    }
    
    func getData(url: URL?) -> Promise<[episode]> {
      
        return firstly {
            URLSession.shared.dataTask(.promise, with: url!)
        }.compactMap {
            return try JSONDecoder().decode([episode].self, from: $0.data)
        }
    }
}

