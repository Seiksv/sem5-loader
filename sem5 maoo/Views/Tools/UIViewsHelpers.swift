//
//  UIViewsHelpers.swift
//  sem5 maoo
//
//  Created by Seiks on 5/3/22.
//

import Foundation
import UIKit

class UIViewsHelpers {
    static func createStackView(distribution: UIStackView.Distribution, axis: NSLayoutConstraint.Axis, spacing: Int) -> UIStackView {
        let stackView = UIStackView()
        let tap = UITapGestureRecognizer()
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = distribution
        stackView.axis = axis
        stackView.addGestureRecognizer(tap)
        stackView.isUserInteractionEnabled = true
        stackView.spacing = CGFloat(spacing)
        
        return stackView
    }
    
    static func createLabel (title: String, height: Int, tag: Int, size: Int, font: String, align: NSTextAlignment ,controller: UIViewController) -> UILabel {
        let label = UILabel()
        let tap = UITapGestureRecognizer(target: controller, action: #selector(MasterViewController.goToDifferentView(_:)))
        
        label.text = title
        label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        label.textColor = UIColor.black
        label.textAlignment = align
        label.font = UIFont(name: font, size: CGFloat(size))
        label.alpha = 1.0
        label.tag = tag
        label.heightAnchor.constraint(greaterThanOrEqualToConstant: CGFloat(height)).isActive = true
        label.numberOfLines = 10
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(tap)
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        return label
    }
    
    static func createImageView (container: UIView) -> UIImageView {
       
        let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 100, height: container.frame.height/2))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.heightAnchor.constraint(equalToConstant: CGFloat(200)).isActive = true
        imageView.image = UIImage(named: "nomoon")
        return imageView
    }
}



