//
//  DetailViewController.swift
//  sem5 maoo
//
//  Created by Seiks on 3/3/22.
//

import Foundation
import UIKit
import PromiseKit

class DetailViewController: UIViewController   {
  
    @IBOutlet weak var activityLoader: UIActivityIndicatorView!
    
    let episodes = Episodes()
    var scrollView = UIScrollView()
    var stackView = UIStackView()
    var episodeImage = UIImageView()
    var dataInfo: Episodes.episode?
    
    override func viewDidLoad() {
        super.viewDidLoad()
            
        if let data = MasterViewController.selectedData {
            createContenScrollView ()
            episodeImage = UIViewsHelpers.createImageView(container: stackView)
            setDataInDetailView(data: data)
        }
    }
    
    func createContenScrollView () {
        view.addSubview(scrollView)
        scrollView.pin(to: view)
        scrollView.alwaysBounceVertical = true
        scrollView.automaticallyAdjustsScrollIndicatorInsets = true
        stackView = UIViewsHelpers.createStackView(distribution: .fill, axis: .vertical, spacing: 10)
        scrollView.addSubview(stackView)
        stackView.pin(to: scrollView)
        stackView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        stackView.isLayoutMarginsRelativeArrangement = true
    }
    
    func setDataInDetailView(data: Episodes.episode){
        
        let imageHelper = ImageHelper()
        activityLoader.startAnimating()
        if let image = data.image {
            
            let imageProcess = imageHelper.getImage(imageURL: URL(string: image)!)
            imageProcess
                .then({ proccessingData -> Promise<UIImage> in
                    self.activityLoader.stopAnimating()
                    return Promise.value(proccessingData)
                })
                .done({ [weak self] icon in
                    self?.episodeImage.image = icon
                })
                .catch({ error in
                    print(error)
                })
        } else {
            episodeImage.image = UIImage(named: "noImage")
            Toast.show(message: StringConstants.noImage, controller: self)
            print(StringConstants.noImage)
        }
        
        stackView.addArrangedSubview(episodeImage)
        episodeImage.pin(to: stackView)
        
        let titleLabel = UIViewsHelpers.createLabel(title: StringConstants.title, height: 20, tag: 0, size: 12, font: StringConstants.helveticaFont, align: .center, controller: self)
        let title = UIViewsHelpers.createLabel(title: data.title, height: 30, tag: 0, size: 20, font: StringConstants.hiraginoFont,  align: .center, controller: self)
        let dscriptionLabel = UIViewsHelpers.createLabel(title: "Descripcion: ", height: 20, tag: 0, size: 12, font: StringConstants.helveticaFont,  align: .center, controller: self)
        let dscription = UIViewsHelpers.createLabel(title: data.description, height: 150, tag: 0, size: 18, font: StringConstants.hiraginoFont, align: .center, controller: self)
        let dateLabel = UIViewsHelpers.createLabel(title: "Fecha: ", height: 20, tag: 0, size: 12, font: StringConstants.helveticaFont, align: .center, controller: self)
        let date = UIViewsHelpers.createLabel(title: data.date, height: 20, tag: 0, size: 18, font: StringConstants.hiraginoFont, align: .center, controller: self)
        let firstLocationLabel = UIViewsHelpers.createLabel(title: "Locacion 1: ", height: 20, tag: 0, size: 12, font: StringConstants.helveticaFont, align: .center, controller: self)
        let firstLocation = UIViewsHelpers.createLabel(title: data.locationline1, height: 20, tag: 0, size: 18, font: StringConstants.hiraginoFont, align: .center, controller: self)
        let secondLocationLabel = UIViewsHelpers.createLabel(title: "Locacion 2: ", height: 20, tag: 0, size: 12, font: StringConstants.helveticaFont, align: .center, controller: self)
        let secondLocation = UIViewsHelpers.createLabel(title: data.locationline2, height: 20, tag: 0, size: 18, font: StringConstants.hiraginoFont, align: .center, controller: self)
        let whiteSpace = UIViewsHelpers.createLabel(title: " ", height: 20, tag: 0, size: 18, font: " ", align: .center, controller: self)
        let divider = UIViewsHelpers.createLabel(title: " ", height: 5, tag: 0, size: 18, font: StringConstants.hiraginoFont, align: .center, controller: self)
        divider.backgroundColor = .black
        
        stackView.addArrangedSubview(divider)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(title)
        stackView.addArrangedSubview(dscriptionLabel)
        stackView.addArrangedSubview(dscription)
        stackView.addArrangedSubview(dateLabel)
        stackView.addArrangedSubview(date)
        stackView.addArrangedSubview(firstLocationLabel)
        stackView.addArrangedSubview(firstLocation)
        stackView.addArrangedSubview(secondLocationLabel)
        stackView.addArrangedSubview(secondLocation)
        stackView.addArrangedSubview(whiteSpace)
        stackView.layoutMargins = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        dscription.setNeedsDisplay()
        dscription.setNeedsLayout()
    }
}
