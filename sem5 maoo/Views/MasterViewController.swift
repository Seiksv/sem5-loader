    //
    //  ViewController.swift
    //  sem5 maoo
    //
    //  Created by Seiks on 2/3/22.
    //

import UIKit
import PromiseKit
import Reachability

class MasterViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    let episodes = Episodes()
    let reachability = try! Reachability()
    var scrollView = UIScrollView()
    var stackView = UIStackView()
    var isConnected = true;
    var isLandscape = false;
    var apiUrl: URL?
    var dataInfo: [Episodes.episode] = [.init(id: 0, description: "0", title: "0", timestamp: "0", image: "0", date: "0", locationline1: "0", locationline2: "0")]
    static var selectedData: Episodes.episode? = .init(id: 0, description: "0", title: "0", timestamp: "0", image: "0", date: "0", locationline1: "0", locationline2: "0")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader.startAnimating()
        apiUrl = URL(string:  StringConstants.jsonDataURL)
        
        if let _ = apiUrl {
            
            createContenScrollView()
            getPosition()
            
            if reachability.connection != .unavailable {
                print(StringConstants.conectionSuccess)
                isConnected = true
                fetchData(hasNetwork: isConnected)
                
            } else {
                print(StringConstants.conectionFailure)
                isConnected = false
                fetchData(hasNetwork: isConnected)
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        stackView.subviews.forEach { subview in
            subview.removeFromSuperview()
        }
        
        coordinator.animate(alongsideTransition: nil) { _ in
            self.getPosition()
            self.fetchData(hasNetwork: self.isConnected)
        }
    }
    
    private func getPosition() {
        if UIWindow.isLandscape {
            isLandscape = true;
        } else {
            isLandscape = false;
        }
    }
    
    private func fetchData(hasNetwork: Bool) {
        let episodesManager = EpisodeCache(cacheKey: "episodes")
        
        if !hasNetwork {
            if let person = episodesManager.getEpisodes()  {
                loader.stopAnimating()
                self.generateContent(data: person)
                Toast.show(message: StringConstants.contentLoadByCache, controller: self)
            } else {
                print(StringConstants.contentLoadFailureByCache)
            }
        } else {
            episodes.getData(url: apiUrl)
                .then { [weak self] customData -> Promise<[Episodes.episode]> in
                    
                    return Promise.value(customData)
                }
                .done(on: DispatchQueue.main) { data in
                    self.loader.stopAnimating()
                    self.generateContent(data: data)
                    try? episodesManager.set(episode: data)
                    
                    Toast.show(message: StringConstants.contentLoadByInternet, controller: self)
                }
                .catch { error in
                    print(error)
                }
                .finally {
                    print("done")
                }
        }
    }
    
    func generateContent(data: [Episodes.episode]){
        
        if !isLandscape {
            stackView.axis = .vertical
            
            data.enumerated().forEach{ index, row in
                dataInfo.append(row)
                let episode = UIViewsHelpers.createStackView(distribution: .fill, axis: .vertical, spacing: -5)
                episode.addArrangedSubview((UIViewsHelpers.createLabel(title: StringConstants.title, height: 20, tag: index, size: 12, font: StringConstants.helveticaFont, align: .left, controller: self)))
                episode.addArrangedSubview((UIViewsHelpers.createLabel(title: row.title, height: 50, tag: index, size: 18, font: StringConstants.hiraginoFont, align: .left, controller: self)))
                stackView.addArrangedSubview(episode)
            }
        } else {
            let firstColumnStackView = UIStackView()
            let secondColumnStackView = UIStackView()
            let dataCount = data.count - 1
            firstColumnStackView.axis  = .vertical
            secondColumnStackView.axis  = .vertical
            firstColumnStackView.distribution = .fillEqually
            secondColumnStackView.distribution = .fillEqually
            
            stackView.axis = .horizontal
            
            data.enumerated().forEach{ index, row in
                dataInfo.append(row)
                let episode = UIViewsHelpers.createStackView(distribution: .fill, axis: .vertical, spacing: -5)
                episode.addArrangedSubview((UIViewsHelpers.createLabel(title: StringConstants.title, height: 20, tag: index, size: 12, font: StringConstants.helveticaFont, align: .left, controller: self)))
                episode.addArrangedSubview((UIViewsHelpers.createLabel(title: row.title, height: 50, tag: index, size: 18, font: StringConstants.hiraginoFont, align: .left, controller: self)))
                
                if index <= dataCount/2 {
                    firstColumnStackView.addArrangedSubview(episode)
                } else {
                    secondColumnStackView.addArrangedSubview(episode)
                }
            }
            
            stackView.addArrangedSubview(firstColumnStackView)
            stackView.addArrangedSubview(secondColumnStackView)
        }
    }
    
    func createContenScrollView () {
        view.addSubview(scrollView)
        scrollView.pin(to: view)
        scrollView.alwaysBounceVertical = true
        scrollView.automaticallyAdjustsScrollIndicatorInsets = true
        stackView = UIViewsHelpers.createStackView(distribution: .equalSpacing, axis: .vertical, spacing: 20)
        scrollView.addSubview(stackView)
        stackView.pin(to: scrollView)
        stackView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    }
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        if reachability.connection == .wifi || reachability.connection == .cellular {
            print(StringConstants.conectionSuccess)
            isConnected = true
            Toast.show(message: StringConstants.conectionSuccess, controller: self)
        } else {
            print(StringConstants.conectionFailure)
            isConnected = false
            Toast.show(message: StringConstants.conectionFailure, controller: self)
        }
    }
    
    @objc func goToDifferentView(_ sender : UITapGestureRecognizer? = nil) {
        if let senderView = sender , let view = senderView.view {
            if let labelTag = (view as? UILabel)?.tag {
                
                MasterViewController.selectedData = dataInfo[labelTag+1]
                self.performSegue(withIdentifier: "toDetail", sender: self)
            }
        }
    }
}


